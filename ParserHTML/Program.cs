﻿using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace ParserHTML
{
    class Program
    {
        static void Main()
        {            
            int countURL = 0;            
            string pattern = @"https?://[^\s""'>]+";
            Console.Write("Введите URL-адрес, например https://otus.ru/: ");
            string url = Console.ReadLine();
            if (!string.IsNullOrEmpty(url))
            {
                try
                {
                    var content = new WebClient { Encoding = Encoding.UTF8 }.DownloadString(url);
                    Console.WriteLine("Список URL:");
                    foreach (var match in Regex.Matches(content, pattern))
                    {
                        countURL++;
                        Console.WriteLine(match);
                    }
                    Console.WriteLine($"Всего: {countURL}");
                }
                catch (WebException)
                {
                    Console.WriteLine($"Некорректный URL-адрес либо доступ запрещен");
                }
            }
            else
                Console.WriteLine($"Некорректный URL-адрес");

            Console.ReadLine();
        }
    }
}
